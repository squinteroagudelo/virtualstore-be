from django.db import models
from .order import Order
from .product import Product


class OrderDetail(models.Model):
    order = models.ForeignKey(Order, related_name='OrderId', on_delete=models.CASCADE)
    product = models.ForeignKey(Product, related_name='ProductId', on_delete=models.CASCADE)
    quantity = models.IntegerField(default=0, verbose_name='Quantity')
    discount = models.DecimalField(max_digits=5, decimal_places=2, default=0.0, verbose_name='Discount')

    class Meta:
        db_table = 'OrderDetail'
        verbose_name = 'Detalle Pedido'
        verbose_name_plural = 'Detalle Pedidos'
        ordering = ['id']
