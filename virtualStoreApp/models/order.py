from django.db import models
from .customer import Customer
from .user import User


class Order(models.Model):
    id = models.IntegerField(primary_key=True, verbose_name='OrderId')
    customer = models.ForeignKey(Customer, related_name='CustomerOrder', on_delete=models.CASCADE)
    user = models.ForeignKey(User, related_name='UserOrder', on_delete=models.SET_NULL, null=True)
    order_date = models.DateField(auto_now_add=True, verbose_name='OrderDate')

    class Meta:
        db_table = 'Order'
        verbose_name = 'Pedido'
        verbose_name_plural = 'Pedidos'
        ordering = ['id']
