from django.db import models


class Supplier(models.Model):
    id = models.IntegerField(primary_key=True)
    supplier_name = models.CharField(max_length=150, unique=True, verbose_name='SupplierName')
    supplier_phone = models.CharField(max_length=10, verbose_name='SupplierPhone')

    def __str__(self):
        return self.supplier_name

    class Meta:
        db_table = 'Supplier'
        verbose_name = 'Proveedor'
        verbose_name_plural = 'Proveedores'
        ordering = ['id']
