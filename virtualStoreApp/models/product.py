from django.db import models
from .category import Category
from .supplier import Supplier


class Product(models.Model):
    id = models.IntegerField(primary_key=True)
    product_name = models.CharField(max_length=150, verbose_name='ProductName')
    category = models.ForeignKey(Category, related_name='CategoryProduct', on_delete=models.SET_NULL, null=True)
    supplier = models.ForeignKey(Supplier, related_name='SupplierProduct', on_delete=models.SET_NULL, null=True)
    unit_price = models.DecimalField(max_digits=7, decimal_places=2, default=0.0, verbose_name='ProductPrice')
    product_quantity = models.IntegerField(default=0, verbose_name='ProductQuantity')

    def __str__(self):
        return self.product_name

    class Meta:
        db_table = 'Product'
        verbose_name = 'Producto'
        verbose_name_plural = 'Productos'
        ordering = ['id']
