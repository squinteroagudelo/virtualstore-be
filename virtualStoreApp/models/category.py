from django.db import models


class Category(models.Model):
    category_name = models.CharField(max_length=100, unique=True, verbose_name='CategoryName')

    def __str__(self):
        return self.category_name

    class Meta:
        db_table = 'Category'
        verbose_name = 'Categoría'
        verbose_name_plural = 'Categorías'
        ordering = ['category_name']
