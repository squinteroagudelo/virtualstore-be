from django.db import models
from .user import User


class Account(models.Model):
    user = models.ForeignKey(User, related_name='account', on_delete=models.CASCADE)
    lastChangeDate = models.DateField()
    isActive = models.BooleanField(default=True)

    class Meta:
        db_table = 'Account'
        verbose_name = 'Cuenta'
        verbose_name_plural = 'Cuentas'
        ordering = ['id']
