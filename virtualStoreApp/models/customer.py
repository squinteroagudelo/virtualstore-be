from django.db import models


class Customer(models.Model):
    id = models.BigIntegerField(primary_key=True, verbose_name='Id')
    customer_name = models.CharField(max_length=70, verbose_name='CustomerName')
    customer_lastname = models.CharField(max_length=70, verbose_name='CustomerLastname')
    customer_address = models.CharField(max_length=244, verbose_name='CustomerAddress')
    customer_phone = models.CharField(max_length=10, verbose_name='CustomerPhone')

    def __str__(self):
        return f'{self.customer_name} {self.customer_lastname}'

    class Meta:
        db_table = 'Customer'
        verbose_name = 'Cliente'
        verbose_name_plural = 'Clientes'
        ordering = ['id']
