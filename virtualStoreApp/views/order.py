from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from virtualStoreApp.models import Order
from virtualStoreApp.serializers import OrderSerializer


class OrderViewSet(viewsets.ModelViewSet):
    queryset = Order.objects.all()
    serializer_class = OrderSerializer
    permission_classes = (IsAuthenticated,)
