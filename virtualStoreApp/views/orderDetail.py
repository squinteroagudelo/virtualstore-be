from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from virtualStoreApp.models import OrderDetail
from virtualStoreApp.serializers import OrderDetailSerializer


class OrderDetailViewSet(viewsets.ModelViewSet):
    queryset = OrderDetail.objects.all()
    serializer_class = OrderDetailSerializer
    permission_classes = (IsAuthenticated,)
