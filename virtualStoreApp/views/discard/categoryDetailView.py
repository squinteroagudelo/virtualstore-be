from rest_framework import generics
from rest_framework.permissions import IsAuthenticated

from virtualStoreApp.models import Category
from virtualStoreApp.serializers import CategorySerializer


class CategoryDetailView(generics.RetrieveAPIView):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAuthenticated,)

    def get(self, request, *args, **kwargs):
        return super().get(request, *args, **kwargs)
