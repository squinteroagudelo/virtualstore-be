from rest_framework import views, status
from rest_framework.response import Response
from virtualStoreApp.serializers import CategorySerializer


class CategoryCreateView(views.APIView):

    def post(self, request, *args, **kwargs):
        serializer = CategorySerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.save()

        return Response({'detail': 'Category successfully created'}, status=status.HTTP_201_CREATED)
