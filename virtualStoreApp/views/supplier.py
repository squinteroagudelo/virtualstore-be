from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from virtualStoreApp.models import Supplier
from virtualStoreApp.serializers import SupplierSerializer


class SupplierViewSet(viewsets.ModelViewSet):
    queryset = Supplier.objects.all()
    serializer_class = SupplierSerializer
    permission_classes = (IsAuthenticated,)
