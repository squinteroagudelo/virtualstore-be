from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from virtualStoreApp.serializers.categorySerializer import CategorySerializer
from virtualStoreApp.models.category import Category


class CategoryViewSet(viewsets.ModelViewSet):
    queryset = Category.objects.all()
    serializer_class = CategorySerializer
    permission_classes = (IsAuthenticated,)
