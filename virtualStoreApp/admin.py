from django.contrib import admin
from virtualStoreApp.models import Account
from virtualStoreApp.models import User
from virtualStoreApp.models import Category
from virtualStoreApp.models import Customer
from virtualStoreApp.models import Supplier
from virtualStoreApp.models import Product
from virtualStoreApp.models import Order
from virtualStoreApp.models import OrderDetail

# Register your models here.
admin.site.register(User)
admin.site.register(Account)
admin.site.register(Category)
admin.site.register(Customer)
admin.site.register(Supplier)
admin.site.register(Product)
admin.site.register(Order)
admin.site.register(OrderDetail)
