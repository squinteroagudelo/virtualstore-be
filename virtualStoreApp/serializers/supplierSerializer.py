from rest_framework import serializers
from virtualStoreApp.models import Supplier


class SupplierSerializer(serializers.ModelSerializer):
    class Meta:
        model = Supplier
        fields = ["id", "supplier_name", "supplier_phone"]

    def to_representation(self, obj):
        supplier = Supplier.objects.get(id=obj.id)
        return {
            'code': supplier.id,
            'name': supplier.supplier_name,
            'phone': supplier.supplier_phone,
        }
