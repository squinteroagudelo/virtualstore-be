from rest_framework import serializers
from virtualStoreApp.models.user import User
from virtualStoreApp.models.account import Account


class AccountSerializer(serializers.ModelSerializer):
    class Meta:
        model = Account
        fields = ["id", "lastChangeDate", "isActive"]

    def to_representation(self, obj):
        account = Account.objects.get(id=obj.id)
        user = User.objects.get(id=account.user_id)
        return {
            'id': account.id,
            'lastChangeDate': account.lastChangeDate,
            'isActive': account.isActive,
            'user': {
                'id': user.id,
                'name': user.name,
                'lastname': user.lastname,
                'username': user.username,
                'email': user.email,
            }
        }
