from rest_framework import serializers
from virtualStoreApp.models import Category


class CategorySerializer(serializers.ModelSerializer):
    class Meta:
        model = Category
        fields = ["id", "category_name"]

    def to_representation(self, obj):
        category = Category.objects.get(id=obj.id)
        return {
            'code': category.id,
            'category': category.category_name,
        }
