from rest_framework import serializers
from virtualStoreApp.models import OrderDetail, Order, Product, User, Customer, Category, Supplier


class OrderDetailSerializer(serializers.ModelSerializer):
    class Meta:
        model = OrderDetail
        fields = ["id", "order", "product", "quantity", "discount"]

    def to_representation(self, obj):
        order_details = OrderDetail.objects.get(id=obj.id)
        order = Order.objects.get(id=order_details.order_id)
        user = User.objects.get(id=order.user_id)
        customer = Customer.objects.get(id=order.customer_id)
        product = Product.objects.get(id=order_details.product_id)
        category = Category.objects.get(id=product.category_id)
        supplier = Supplier.objects.get(id=product.supplier_id)
        return {
            'id': order_details.id,
            'order': {
                'id': order.id,
                'date': order.order_date,
                'user': {
                    'username': user.username,
                },
                'customer': {
                    'name': f'{customer.customer_name} {customer.customer_lastname}',
                    'address': customer.customer_address,
                    'phone': customer.customer_phone,
                }
            },
            'product': {
                'id': product.id,
                'name': product.product_name,
                'category': {
                    'name': category.category_name
                },
                'supplier': {
                    'name': supplier.supplier_name,
                    'phone': supplier.supplier_phone
                },
                'unit_price': product.unit_price,
            },
            'quantity': order_details.quantity,
            'discount': order_details.discount,
        }
