from rest_framework import serializers
from virtualStoreApp.models import Customer


class CustomerSerializer(serializers.ModelSerializer):
    class Meta:
        model = Customer
        fields = ["id", "customer_name", "customer_lastname", "customer_address", "customer_phone"]

    def to_representation(self, obj):
        customer = Customer.objects.get(id=obj.id)
        return {
            'id': customer.id,
            'name': f'{customer.customer_name} {customer.customer_lastname}',
            'address': customer.customer_address,
            'phone': customer.customer_phone,
        }
