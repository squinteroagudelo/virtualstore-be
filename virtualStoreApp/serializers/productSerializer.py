from rest_framework import serializers
from virtualStoreApp.models import Product, Category, Supplier
from virtualStoreApp.serializers import CategorySerializer, SupplierSerializer


class ProductSerializer(serializers.ModelSerializer):

    class Meta:
        model = Product
        fields = ["id", "product_name", "category", "supplier", "unit_price", "product_quantity"]

    def to_representation(self, obj):
        product = Product.objects.get(id=obj.id)
        category = Category.objects.get(id=product.category_id)
        supplier = Supplier.objects.get(id=product.supplier_id)
        return {
            'id': product.id,
            'name': product.product_name,
            'category': {
                'name': category.category_name
            },
            'supplier': {
                'name': supplier.supplier_name,
                'phone': supplier.supplier_phone
            },
            'unit_price': product.unit_price,
            'quantity': product.product_quantity,
        }
