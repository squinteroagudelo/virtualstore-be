from rest_framework import serializers
from virtualStoreApp.models import Order, User, Customer


class OrderSerializer(serializers.ModelSerializer):
    class Meta:
        model = Order
        fields = ["id", "customer", "user", "order_date"]

    def to_representation(self, obj):
        order = Order.objects.get(id=obj.id)
        user = User.objects.get(id=order.user_id)
        customer = Customer.objects.get(id=order.customer_id)
        return {
            'id': order.id,
            'customer': {
                'name': f'{customer.customer_name} {customer.customer_lastname}',
                'address': customer.customer_address,
                'phone': customer.customer_phone,
            },
            'user': {
                'username': user.username,
            },
            'date': order.order_date,
        }
