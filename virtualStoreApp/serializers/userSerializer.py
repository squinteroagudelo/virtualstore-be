from rest_framework import serializers
from virtualStoreApp.models import Account
from virtualStoreApp.models.user import User
from virtualStoreApp.serializers.accountSerializer import AccountSerializer


class UserSerializer(serializers.ModelSerializer):
    account = AccountSerializer()

    class Meta:
        model = User
        fields = ["id", "username", "password", "name", "lastname", "email", "account"]

    def create(self, validated_data):
        account_data = validated_data.pop('account')
        user_instance = User.objects.create(**validated_data)
        Account.objects.create(user=user_instance, **account_data)
        return user_instance

    def to_representation(self, obj):
        user = User.objects.get(id=obj.id)
        account = Account.objects.get(user=obj.id)

        return {
            'id': user.id,
            'username': user.username,
            'name': user.name,
            'lastname': user.lastname,
            'email': user.email,
            'account': {
                'id': account.id,
                'lastChangeDate': account.lastChangeDate,
                'isActive': account.isActive
            }
        }
