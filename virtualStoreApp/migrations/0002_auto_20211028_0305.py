# Generated by Django 3.2.8 on 2021-10-28 08:05

from django.db import migrations


class Migration(migrations.Migration):

    dependencies = [
        ('virtualStoreApp', '0001_initial'),
    ]

    operations = [
        migrations.RenameField(
            model_name='orderdetail',
            old_name='order_id',
            new_name='order',
        ),
        migrations.RenameField(
            model_name='orderdetail',
            old_name='product_id',
            new_name='product',
        ),
    ]
