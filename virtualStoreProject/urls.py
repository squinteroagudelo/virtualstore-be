"""virtualStoreProject URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from rest_framework.routers import DefaultRouter
from rest_framework_simplejwt.views import TokenObtainPairView, TokenRefreshView

from virtualStoreApp import views
from virtualStoreApp.views import category, customer, supplier, product, order, orderDetail

router = DefaultRouter()
router.register(r'categories', category.CategoryViewSet, basename='category')
router.register(r'customers', customer.CustomerViewSet, basename='customer')
router.register(r'suppliers', supplier.SupplierViewSet, basename='supplier')
router.register(r'products', product.ProductViewSet, basename='product')
router.register(r'orders', order.OrderViewSet, basename='order')
router.register(r'order_details', orderDetail.OrderDetailViewSet, basename='order_detail')


urlpatterns = [
    path('', include(router.urls)),
    path('admin/', admin.site.urls),
    path('login/', TokenObtainPairView.as_view()),
    path('refresh/', TokenRefreshView.as_view()),
    path('user/new/', views.UserCreateView.as_view()),
    path('user/<int:pk>/', views.UserDetailView.as_view()),
]
